#!/usr/bin/env bash
sudo apt-add-repository --yes --update ppa:ansible/ansible
#sudo apt-get upgrade -y
#sudo apt-get update -y
sudo apt-get install software-properties-common ssh ansible cowsay python3-pexpect -y
sudo ln -s /usr/games/cowsay /usr/bin/cowsay
