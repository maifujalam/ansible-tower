#!/bin/bash
FILE=/home/vagrant/.ssh/id_rsa.pub
if [ ! -f "$FILE" ]; then
    cowsay "rsa key not found.Generating rsa key. \n"
    ssh-keygen -t rsa -N "" -f /home/vagrant/.ssh/id_rsa
    echo "whoami"
    whoami
    echo "did exit from root above"
    cp /vagrant/Secrets/id* /home/vagrant/.ssh
    sudo chown vagrant:vagrant /home/vagrant/.ssh/*
    ls -al /home/vagrant/.ssh
    systemctl restart sshd
    systemctl status sshd
else
    cowsay "RSA Key present."
    ls /home/vagrant/.ssh/
    cowsay "RSA Key  Found.Force Generating rsa key. \n"
    ssh-keygen -t rsa -N "" -f /home/vagrant/.ssh/id_rsa
    echo "whoami"
    whoami
    echo "did exit from root above"
    cp /vagrant/Secrets/id* /home/vagrant/.ssh
    sudo chown vagrant:vagrant /home/vagrant/.ssh/*
    ls -al /home/vagrant/.ssh
    systemctl restart sshd
    systemctl status sshd
fi
#sudo echo "yes \n" | sudo ssh-copy-id -i /home/vagrant/.ssh/id_rsa.pub vagrant@Slave1
#ssh-copy-id -i /home/vagrant/.ssh/id_rsa -o StrictHostKeyChecking=no Slave1