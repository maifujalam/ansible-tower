#!/bin/bash
FILE=/home/vagrant/.ssh/id_rsa.pub
if [ ! -f "$FILE" ]; then
    cowsay "RSA Key not found.Generating RSA Key. \n"
    ssh-keygen -t rsa -N "" -f /home/vagrant/.ssh/id_rsa
    echo "whoami"
    whoami
    echo "did exit from root above"
    cp /vagrant/Secrets/id* /home/vagrant/.ssh
    sudo chown vagrant:vagrant /home/vagrant/.ssh/*
    ls -al /home/vagrant/.ssh
    sudo systemctl restart sshd
    sudo systemctl status sshd
else
    cowsay "RSA Key present.Removing old RSA Key"
    sudo rm ~/.ssh/id*
    cowsay "Generating new RSA Key. \n"
    ssh-keygen -t rsa -N "" -f /home/vagrant/.ssh/id_rsa
    echo "whoami"
    whoami
    echo "did exit from root above"
    cp /vagrant/Secrets/id* /home/vagrant/.ssh
    sudo chown vagrant:vagrant /home/vagrant/.ssh/*
    ls -al /home/vagrant/.ssh
    sudo systemctl restart sshd
    sudo systemctl status sshd

      ls /home/vagrant/.ssh/
fi
#sudo echo "yes \n" | sudo ssh-copy-id -i /home/vagrant/.ssh/id_rsa.pub vagrant@Slave1
#ssh-copy-id -i /home/vagrant/.ssh/id_rsa -o StrictHostKeyChecking=no Slave1